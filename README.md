# `lxd-containers.nix` // NixOS LXD declarative management

This NixOS module allows defining and managing [LXD](https://linuxcontainers.org/lxd/introduction/#LXD) projects, profiles, images and containers,
from LXD remote images, raw tarballs or [NixOS](https://nixos.org/) containers with inline configuration.

## Features

 - Declarative project/profile/image/container management
   + fully declarative : can be bootstrapped without `lxd init`
   + whole life-cycle managed as a systemd service
   + allow "persistent" objects (won't be deleted)
   + allow "cached" objects (not rebuilt if not changed)
   + AppArmor/SELinux/seccomp integrations
 - Agentless container monitoring
   + using systemd by default, can be customized for other inits
   + integrated in container's service life-cycle
 - OS integrations
   + Debian container generation (debootstrap derivation)
   + NixOS container generation (with shared store)

## Motivations

 - NixOS is a great foundation for declarative infrastructure management
 - NixOS containers provide low-cost service separation, but they are not 
   unprivileged and do not provide a sufficient level of isolation
 - On top of that, NixOS containers cannot be used to run other operating
   systems in the container, so they can not be directly used to interface
   with the non-Nix world
 - LXD allows us to use containers for both service separation and isolation, 
   supporting other operating systems while still providing similar features
   as NixOS containers where relevant.
 - This also provides base images for other operating systems, using the LXD
   standard image store, and allows easy integration with other LXD features.

Related projects:
 - [NixOS containers](https://nixos.org/nixos/manual/#ch-containers) (only for other NixOS instances, uses systemd-nspawn and privileged containers)
 - [NixCloud containers](https://github.com/nixcloud/nixcloud-container/) (only for other NixOS instances, but uses LXC and unprivileged containers)
 - [nixops](https://nixos.org/nixops/) NixOS configuration deployment tool

## Targeted guest operating systems:

 - Nixos 19.09 `Loris`
 - Debian 9 `Stretch`
 - Debian 10 `Buster`
 - *TODO* Alpine 3.10+

## Current status

Known issues:
 - **WIP, interface may change**
 - **no maintenance guarantee**
 - no networking integration
 - no soft rebuild / no atomic switch
 - missing auto-restart for some LXD config options
 - limited AppArmor hardening
 - no SELinux hardening
 - no seccomp hardening

## Example

```
config.virtualisation.lxd = {
  enable = true;
  projects.default.containers = {
    test-debian = {
      image = { type = "lxd"; lxdImage = "debian/9"; };
      config.devices.nic = {
        type = "nic"; nictype = "bridged"; parent = "lxdbr0";
      };
    };
    test-nixos.image = {
      type = "nixos"; nixosShareStore = false;
      nixosConfig = { config, ...}: {
        config.networking.hostName = "test";
      };
    };
  };    
};
```